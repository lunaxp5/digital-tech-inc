import React from "react";

export const UserContext = React.createContext(); // primero creamos el contexto

export const UserProvider = (props) => {
  // luego creamos el provider
  const [user, setUser] = React.useState({
    username: "",
    name: "",
    surname: "",
    avatar: "",
  }); // definimos la variable que vamos a mantener y pasar a los componentes, en este caso quiero un JSON

  return (
    <UserContext.Provider value={[user, setUser]}>
      {/* aqui pasamos como valor el mismo arreglo de datos a mantener  
           ############################################################################
           # NOTA: este provider es el que va a envolver al padre de los componentes, #
           # El context es el que invocaremos como un 'State' de react                #
           ############################################################################ 
      */}
      {props.children}
    </UserContext.Provider>
  );
};
