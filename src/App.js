import { Login, CreateAccount, Home } from "./Views";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import "./Theme/reset.scss";
import "./Theme/global.scss";
import React from "react";
import { UserProvider } from "./Context/User";

const PrivateRoute = ({ children, isAuth, ...rest }) => {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        isAuth ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
};

function App() {
  const [isAuth, setIsAuth] = React.useState(false);
  const [user, setUser] = React.useState({});
  const isLogin = () => {
    const data = localStorage.getItem("user");
    if (data !== "" && data !== null && data !== undefined) {
      setIsAuth(true);
      setUser(JSON.parse(data));
    } else {
      setIsAuth(false);
      setUser({});
    }
  };
  React.useEffect(() => {
    isLogin();
  }, []);

  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/registro">
            <CreateAccount />
          </Route>
          <PrivateRoute path="/" isAuth={isAuth}>
            <UserProvider>
              <Home user={user} />
            </UserProvider>
          </PrivateRoute>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
