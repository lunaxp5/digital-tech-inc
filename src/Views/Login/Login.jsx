import React from "react";

import "./login.scss";
import Unlock from "../../Assets/unlock.svg";
import { Link, useHistory } from "react-router-dom";
import { useGetUser } from "../../Hooks";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

const schema = yup
  .object({
    username: yup
      .string()
      .required("Este campo es requerido")
      .min(3, "Muy corto")
      .max(20, "Máximo 20 caracteres"),
  })
  .required();

const Login = () => {
  const history = useHistory();
  const { findUser } = useGetUser();
  const [error, setError] = React.useState("");

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const onSubmit = (data) => {
    findUser(data.username)
      .then((res) => {
        setError("");
        localStorage.setItem("user", JSON.stringify(res));
        window.location.reload();
      })
      .catch((err) => {
        setError("El usuario no existe");
      });
  };
  React.useEffect(() => {
    const data = localStorage.getItem("user");
    if (data !== "" && data !== null && data !== undefined) {
      history.push("/");
    }
  }, []);
  return (
    <>
      <div className="login-wrapper">
        <div className="login-container">
          <div className="col-left">
            <img className="image-unlock" src={Unlock} alt="" />
          </div>
          <div className="col-right">
            <h3 className="title-login">Iniciar sesión</h3>
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="data">
                <label className="form-label">Usuario</label>
                <input
                  {...register("username")}
                  type="text"
                  className={`form-control ${
                    errors.username?.message || error ? "is-invalid" : ""
                  }`}
                  placeholder="Usuario"
                />
                {errors.username?.message ? (
                  <p className="error">{errors.username?.message}</p>
                ) : null}
                {error !== "" ? <p className="error">{error}</p> : null}
              </div>

              <div className="data">
                <div className="d-grid gap-2">
                  <button className="btn btn-primary" type="submit">
                    Ingresar
                  </button>
                  <Link to="/registro" className="link">
                    ¿Aún no tienes cuenta?
                  </Link>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};
export default Login;
