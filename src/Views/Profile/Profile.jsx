import React from "react";
import { useParams } from "react-router";
import { MyFeed } from "../../Containters";
import { UserContext } from "../../Context/User";
import "./profile.scss";

const Profile = () => {
  const [user] = React.useContext(UserContext); // Llamamos el contexto de require
  const [userQuery, setUserQuery] = React.useState("");
  let { username } = useParams();

  React.useEffect(() => {
    if (username) {
      setUserQuery(username);
    } else {
      setUserQuery(user.username);
    }
  }, []);
  return (
    <>
      <div className="profile-wrapper">
        <div className="profile-wrapper">
          <h2>
            <MyFeed username={userQuery} />
          </h2>
        </div>
      </div>
    </>
  );
};
export default Profile;
