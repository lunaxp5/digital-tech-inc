import React from "react";
import { NavBar, Sidebar, Feed } from "../../Containters";
import { Switch, Route } from "react-router-dom";
import { UserContext } from "../../Context/User";
import { Profile, CreatePost } from "..";
import "./home.scss";

const Home = (props) => {
  const [, setUser] = React.useContext(UserContext); // Llamamos el contexto de require

  React.useEffect(() => {
    setUser(props.user);
  }, [props.user]);

  return (
    <>
      <NavBar />

      <div className="app-wrapper">
        <Switch>
          <Route path="/publicar">
            <CreatePost />
          </Route>

          <Route path="/perfil/:username">
            <div className="post-wrapper">
              <Profile />
            </div>
            <Sidebar />
          </Route>

          <Route path="/">
            <div className="post-wrapper">
              <Feed />
            </div>
            <Sidebar />
          </Route>
        </Switch>
      </div>
    </>
  );
};
export default Home;
