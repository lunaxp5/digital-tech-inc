import React from "react";
import { Link, useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";

import avatarPreview from "../../Assets/male-avatar.svg";
import "./createAccount.scss";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import { useGetUser } from "../../Hooks";

const schema = yup
  .object({
    username: yup
      .string()
      .required("Este campo es requerido")
      .min(3, "Muy corto")
      .max(20, "Máximo 20 caracteres"),
    name: yup
      .string()
      .required("Este campo es requerido")
      .min(3, "Muy corto")
      .max(20, "Máximo 20 caracteres"),
    surname: yup
      .string()
      .required("Este campo es requerido")
      .min(3, "Muy corto")
      .max(20, "Máximo 20 caracteres"),
  })
  .required();

const CreateAccount = () => {
  const history = useHistory();
  const [avatar, setAvatar] = React.useState("");
  const [error, setError] = React.useState("");
  const { findUser } = useGetUser();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const upload = (img) => {
    if (img !== null && img !== "" && img !== undefined) {
      const reader = new FileReader();
      reader.readAsDataURL(img);
      reader.onload = function () {
        // devolvemos el archivo
        setAvatar(reader.result); // convertimos el archivo a base 64 y lo devolvemos
      };
      reader.onerror = function (error) {
        console.log("Error: ", error);
      };
    }
  };

  const onSubmit = (data) => {
    data.avatar = avatar;
    let usersArray = [];
    usersArray = localStorage.getItem("users");
    if (usersArray !== "" && usersArray !== null && usersArray !== undefined) {
      usersArray = JSON.parse(usersArray);

      findUser(data.username)
        .then((res) => {
          setError("El nombre de usuario ya existe");
        })
        .catch(() => {
          setError("");
          if (usersArray?.length > 0) {
            usersArray.push(data);
          } else {
            usersArray = [data];
          }
          localStorage.setItem("users", JSON.stringify(usersArray));
          history.push("/login");
        });
    } else {
      findUser(data.username)
        .then((res) => {
          setError("El nombre de usuario ya existe");
        })
        .catch(() => {
          setError("");

          usersArray = [data];
          localStorage.setItem("users", JSON.stringify(usersArray));
          history.push("/login");
        });
    }
  };

  return (
    <>
      <div className="account-wrapper">
        <div className="account-container">
          <div className="col-left">
            <h3 className="title-account">Crear cuenta</h3>
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="data create">
                <label className="form-label">Nombre de usuario </label>
                <input
                  {...register("username")}
                  type="text"
                  className={`form-control ${
                    errors.username?.message || error ? "is-invalid" : ""
                  }`}
                  placeholder="Nombre de usuario"
                />
                {errors.username?.message ? (
                  <p className="error">{errors.username?.message}</p>
                ) : null}
                {error !== "" ? <p className="error">{error}</p> : null}
              </div>
              <div className="data create">
                <label className="form-label">Nombre</label>
                <input
                  type="text"
                  {...register("name")}
                  className={`form-control ${
                    errors.name?.message ? "is-invalid" : ""
                  }`}
                  placeholder="Nombre"
                />
                {errors.name?.message ? (
                  <p className="error">{errors.name?.message}</p>
                ) : null}
              </div>
              <div className="data create">
                <label className="form-label">Apellido</label>
                <input
                  {...register("surname")}
                  type="text"
                  className={`form-control ${
                    errors.surname?.message ? "is-invalid" : ""
                  }`}
                  placeholder="Apellido"
                />
                {errors.surname?.message ? (
                  <p className="error">{errors.surname?.message}</p>
                ) : null}
              </div>

              <div className="data create button">
                <div className="d-grid gap-2">
                  <button type="submit" className="btn btn-primary">
                    Enviar
                  </button>
                </div>
              </div>
            </form>
            <div className="wrapper-link">
              <Link to="/login" className="link">
                Ya tengo cuenta
              </Link>
            </div>
          </div>
          <div className="col-right">
            <label htmlFor="avatar-file" id="avatar">
              <img
                src={avatar !== "" ? avatar : avatarPreview}
                id="avatar-img"
                className="image-profile"
                alt=""
              />
            </label>
            <label htmlFor="avatar-file" id="link-upload-rs">
              Subir foto
            </label>
            <input
              accept="image/*"
              type="file"
              id="avatar-file"
              onChange={(e) => {
                upload(e.target.files[0]);
              }}
            />
          </div>
        </div>
      </div>
    </>
  );
};
export default CreateAccount;
