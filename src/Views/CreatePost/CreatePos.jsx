import React from "react";
import { useForm } from "react-hook-form";
import { Post } from "../../Containters";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { UserContext } from "../../Context/User";
import { useToBase64, useSendPost } from "../../Hooks";
import { useHistory } from "react-router";

import "./createPost.scss";
const schema = yup
  .object({
    message: yup
      .string()
      .required("Este campo es requerido")
      .min(10, "Mínimo 10 caracteres")
      .max(500, "Máximo 500 caracteres"),
    location: yup
      .string()
      .required("Este campo es requerido")
      .min(4, "Mínimo 4 caracteres")
      .max(30, "Máximo 30 caracteres"),
    status: yup.string().required("Este campo es requerido"),
  })
  .required();

const CreatePost = () => {
  let history = useHistory();
  const [user] = React.useContext(UserContext);

  const [img, setImg] = React.useState("");
  const { convert } = useToBase64();
  const [, savePost] = useSendPost();

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const watchAllFields = watch();

  const upload = (img) => {
    if (img) {
      convert(img)
        .then((res) => {
          setImg(res);
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      setImg("");
    }
  };

  const onSubmit = (data) => {
    const date = new Date();

    data.image = img;
    data.create_at = date;
    data.author = user;
    data.likes = [];

    savePost(data);
    history.push("/");
  };

  return (
    <>
      <div className="create-post-wrapper">
        <div className="col-left">
          <div className="card">
            <div className="card-header">Crea tu publicación</div>
            <div className="card-body">
              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="data-post">
                  <div className="mb-3">
                    <label htmlFor="formFile" className="form-label">
                      Cargar Imagen
                    </label>
                    <input
                      className="form-control"
                      onChange={(e) => {
                        upload(e.target.files[0]);
                      }}
                      type="file"
                      id="formFile"
                    />
                  </div>
                </div>
                <div className="data-post">
                  <label className="form-label">Ubicación </label>
                  <input
                    {...register("location")}
                    type="text"
                    className={`form-control ${
                      errors.location?.message ? "is-invalid" : ""
                    }`}
                    placeholder="Ubicación"
                  />
                  {errors.location?.message ? (
                    <p className="error">{errors.location?.message}</p>
                  ) : null}
                </div>

                <div className="data-post">
                  <label className="form-label">Mensaje </label>
                  <textarea
                    rows={3}
                    {...register("message")}
                    type="textatea"
                    className={`form-control ${
                      errors.message?.message ? "is-invalid" : ""
                    }`}
                    placeholder="Nombre de usuario"
                  />
                  {errors.message?.message ? (
                    <p className="error">{errors.message?.message}</p>
                  ) : null}
                </div>

                <div className="data-post">
                  <label className="form-label">Estado de la publicación</label>
                  <select
                    {...register("status")}
                    className={`form-control ${
                      errors.status?.message ? "is-invalid" : ""
                    }`}
                    defaultValue=""
                    aria-label="Default select example"
                  >
                    <option value="">Seleccione</option>
                    <option value="draft">Borrador</option>
                    <option value="published">Publicar</option>
                  </select>
                  {errors.status?.message ? (
                    <p className="error">{errors.status?.message}</p>
                  ) : null}
                </div>
                <div className="data-post button">
                  <button type="submit" className="btn btn-primary">
                    Publicar
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div className="col-right">
          <Post
            avatar={user.avatar}
            fullname={user.name}
            location={watchAllFields.location}
            msg={watchAllFields.message}
            image={img}
            status={watchAllFields.status}
          />
        </div>
      </div>
    </>
  );
};
export default CreatePost;
