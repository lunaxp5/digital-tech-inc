import Login from "./Login/Login";
import CreateAccount from "./CreateAccount/CreateAccount";
import Home from "./Home/Home";
import CreatePost from "./CreatePost/CreatePos";
import Profile from "./Profile/Profile";
export { Login, CreateAccount, Home, CreatePost, Profile };
