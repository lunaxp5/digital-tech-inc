import React from "react";
import { Link } from "react-router-dom";
import Default from "../../Assets/male-avatar.svg";

import "./avatar.scss";

const Avatar = (props) => {
  return (
    <>
      <Link {...props}>
        <img
          className={`avatar-profile ${props?.size ? props?.size : ""}`}
          src={props?.src ? props?.src : Default}
          alt=""
        />
      </Link>
    </>
  );
};
export default Avatar;
