import React from "react";

// estructura de users
//
// avatar: "/static/media/male-avatar.1546c65e.svg"
// name: "Jose Luna"
// surname: "Luna"
// username: "romely"
//

const useGetUser = (userToFind) => {
  const findUser = (username) => {
    return new Promise((resolve, reject) => {
      let usersArray = [];
      usersArray = JSON.parse(localStorage.getItem("users"));

      if (
        usersArray !== null &&
        usersArray !== undefined &&
        usersArray !== ""
      ) {
        usersArray?.map((find, i) => {
          if (find.username === username) {
            resolve(find);
            return true;
          }
          if (usersArray.length - 1 === i) {
            reject({});
            return false;
          }
        });
      } else {
        reject({});
      }
    });
  };

  React.useEffect(() => {
    findUser(userToFind)
      .then((res) => {})
      .catch(() => {});
  }, [userToFind]);

  return { findUser };
};

export default useGetUser;
