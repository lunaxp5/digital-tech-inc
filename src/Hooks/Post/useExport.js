const useExport = (username) => {
  const exportPost = (username) => {
    let data = localStorage.getItem("data");

    if (data) {
      data = JSON.parse(data);

      let aux = data.filter((post) => {
        return post.author.username === username;
      });

      aux = aux.sort((a, b) => {
        return new Date(b.create_at) - new Date(a.create_at);
      });
      const link = document.createElement("a");
      link.href =
        "data:text/plain;charset=UTF-8," +
        encodeURIComponent(JSON.stringify(aux));
      //set default action on link to force download, and set default filename:
      link.download = "MyPost.txt";
      link.click();
    } else {
      alert("no hay post");
    }
  };

  return [exportPost];
};

export default useExport;
