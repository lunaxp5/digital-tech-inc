import React from "react";

const useGetMyPostFeed = (username) => {
  const [postFeed, setPostFeed] = React.useState([]);

  const getPost = (username) => {
    let data = localStorage.getItem("data");

    if (data) {
      data = JSON.parse(data);

      let aux = data.filter((post) => {
        return post.author.username === username;
      });

      aux = aux.sort((a, b) => {
        return new Date(b.create_at) - new Date(a.create_at);
      });

      setPostFeed(aux);
    } else {
      data = "";
      setPostFeed(data);
    }
  };

  React.useEffect(() => {
    getPost(username);
  }, [username]);

  return [postFeed, getPost];
};

export default useGetMyPostFeed;
