import React from "react";

const useGetPostFeed = (username) => {
  const [postFeed, setPostFeed] = React.useState([]);

  const likePost = (params) => {
    let data = localStorage.getItem("data");

    if (data) {
      data = JSON.parse(data);

      for (var i = 0; i < data.length; i++) {
        const fecha = data[i].create_at;
        const autor = data[i].author.username;

        if (fecha === params.create_at && autor === params.author) {
          const arr = data[i].likes.filter((user) => {
            return params.username === user;
          });

          if (arr.length === 0) {
            //No le dio ha dado like
            data[i].likes.push(params.username);
            localStorage.setItem("data", JSON.stringify(data));
          }
        }
      }
      getPost();
    } else {
      data = "";
      setPostFeed(data);
    }
  };

  const getMyPost = (username) => {
    let data = localStorage.getItem("data");

    if (data) {
      data = JSON.parse(data);

      let aux = data.filter((post) => {
        return post.author.username === username;
      });

      aux = aux.sort((a, b) => {
        return new Date(b.create_at) - new Date(a.create_at);
      });

      setPostFeed(aux);
    } else {
      data = "";
      setPostFeed(data);
    }
  };

  const getPost = (username) => {
    let data = localStorage.getItem("data");

    if (data) {
      data = JSON.parse(data);

      let aux = data.filter((post) => {
        return post.status === "published";
      });

      aux = aux.sort((a, b) => {
        return new Date(b.create_at) - new Date(a.create_at);
      });

      setPostFeed(aux);
    } else {
      data = "";
      setPostFeed(data);
    }
  };

  React.useEffect(() => {
    getMyPost(username);
  }, [username]);

  React.useEffect(() => {
    getPost();
  }, []);

  return [postFeed, likePost];
};

export default useGetPostFeed;
