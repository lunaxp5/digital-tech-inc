import React from "react";

const useSendPost = () => {
  const [post, setPost] = React.useState([]);

  const savePost = (post) => {
    let data = localStorage.getItem("data");

    if (data) {
      data = JSON.parse(data);
      data.push(post);
    } else {
      data = [post];
    }
    setPost(data);
    localStorage.setItem("data", JSON.stringify(data));
  };
  return [post, savePost];
};

export default useSendPost;
