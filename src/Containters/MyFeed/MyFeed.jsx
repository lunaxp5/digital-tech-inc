import React from "react";
import { Link } from "react-router-dom";
import { Post } from "..";
import { useGetMyPostFeed } from "../../Hooks";
import "./myfeed.scss";

const MyFeed = ({ username }) => {
  const [postFeed] = useGetMyPostFeed(username);

  return (
    <div>
      {postFeed?.length > 0 ? (
        postFeed.map((post, i) => {
          return (
            <Post
              key={`post-${i}`}
              fullname={post.author.username}
              avatar={post.author.avatar}
              msg={post.message}
              location={post.location}
              image={post.image}
              likes={post?.likes?.length ? post?.likes?.length : 0}
              activeLike={post?.likes?.indexOf(username) > -1}
            />
          );
        })
      ) : (
        <>
          <h3>Aún no hay publicaciones</h3>
          <Link to="/publicar">publicar</Link>
        </>
      )}
    </div>
  );
};
export default MyFeed;
