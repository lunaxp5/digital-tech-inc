import React from "react";
import { UserContext } from "../../Context/User";
import { Avatar } from "../../Components";

import "./sidebar.scss";
import { useExport } from "../../Hooks";

const Sidebar = () => {
  const [user] = React.useContext(UserContext);
  const [exportPost] = useExport();

  const logout = () => {
    if (window.confirm("¿Desea cerrar la sessión?")) {
      localStorage.setItem("user", "");
      window.location.reload();
    }
  };
  return (
    <>
      <div className="sidebar-wrapper">
        <div className="profile-wrapper">
          <div className="info">
            <div className="avatar-container">
              <Avatar size="medium" to={`/perfil/${user.username}`} />
            </div>
            <div className="data-profile">
              <div className="username">{user?.username}</div>
              <div className="name">{user?.name + " " + user?.surname}</div>
            </div>
          </div>

          <div className="actions">
            <div className="d-grid gap-2 col-12 mx-auto">
              <button
                className="btn btn-warning"
                type="button"
                onClick={() => exportPost(user.username)}
              >
                Exportar mis post
              </button>
              <button
                className="btn btn-dark"
                type="button"
                onClick={() => logout()}
              >
                Cerrar sessión
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Sidebar;
