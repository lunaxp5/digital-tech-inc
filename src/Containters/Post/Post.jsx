import React from "react";
import { Avatar } from "../../Components";
import Default from "../../Assets/image-default.svg";
import { Player } from "@lottiefiles/react-lottie-player";
import Animation from "../../Assets/anime/like.json";
import Heart from "../../Assets/heart.svg";
import "./post.scss";

const Post = ({
  fullname,
  avatar,
  location,
  msg,
  image,
  status,
  likes,
  like,
  activeLike,
}) => {
  const [countWord, setCountWord] = React.useState(100);
  const animationRef = React.useRef();

  const readMore = () => {
    setCountWord(msg.length);
  };
  const readLess = () => {
    setCountWord(100);
  };

  const Togglebuttons = () => {
    if (msg?.length > 0) {
      if (countWord === 100 && msg?.length > 100)
        return (
          <button
            onClick={() => {
              readMore();
            }}
            className="more btn btn-link"
            type="button"
          >
            Leer Más..
          </button>
        );
      if (msg?.length > 100) {
        return (
          <button
            onClick={() => {
              readLess();
            }}
            className="more btn btn-link"
            type="button"
          >
            Leer menos..
          </button>
        );
      }
    }
    return null;
  };

  const likeAnimation = () => {
    animationRef?.current?.play();
  };

  return (
    <>
      <div className="card-post">
        <div className="card">
          <div className="card-body">
            <div className="header">
              <div className="avatar">
                <Avatar src={avatar} to={`/perfil/${fullname}`} />
              </div>
              <div className="data">
                <p className="fullname">{fullname}</p>
                <p className="location">{location}</p>
              </div>
              <div className="status-content">
                <span className="status">{status}</span>
              </div>
            </div>

            <div className="body">
              {image ? (
                <div className="image-post-container">
                  <img
                    src={image ? image : Default}
                    alt="postal"
                    className="image-post"
                  />
                </div>
              ) : null}

              <div className="message-container">
                <p className="message">
                  {msg?.slice(0, countWord)}
                  <span>
                    <Togglebuttons />
                  </span>
                </p>
              </div>
              {like ? (
                <div
                  className="like-container"
                  onClick={() => {
                    like();
                    likeAnimation();
                  }}
                >
                  <div className="animation">
                    {activeLike ? (
                      <img className="heart-ic" src={Heart} alt="heart" />
                    ) : (
                      <Player
                        ref={animationRef}
                        autoplay={false}
                        keepLastFrame={true}
                        src={Animation}
                        // style={{ height: "80px", width: "80px" }}
                      ></Player>
                    )}
                  </div>
                  <div className="count-likes">{likes} Me gusta</div>
                </div>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Post;
