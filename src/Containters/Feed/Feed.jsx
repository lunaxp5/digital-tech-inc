import React from "react";
import { Post } from "..";
import { useGetPostFeed } from "../../Hooks";
import { UserContext } from "../../Context/User";
import "./feed.scss";
import { Link } from "react-router-dom";

const Feed = () => {
  const [postFeed, likePost] = useGetPostFeed();
  const [user] = React.useContext(UserContext);

  return (
    <div>
      {postFeed?.length > 0 ? (
        postFeed?.map((post, i) => {
          return (
            <Post
              key={`post-${i}`}
              fullname={post.author.username}
              avatar={post.author.avatar}
              msg={post.message}
              location={post.location}
              image={post.image}
              likes={post?.likes?.length ? post?.likes?.length : 0}
              activeLike={post?.likes?.indexOf(user.username) > -1}
              like={() => {
                likePost({
                  username: user.username,
                  author: post.author.username,
                  create_at: post.create_at,
                });
              }}
            />
          );
        })
      ) : (
        <>
          <h3>Aún no hay publicaciones</h3>
          <Link to="/publicar">publicar</Link>
        </>
      )}
    </div>
  );
};
export default Feed;
